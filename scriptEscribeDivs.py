# Script para escribir la rejilla de divs en el index.html
def calculaPixelLeft(x):
    return x * 10

def calculaPixelTop(y):
    return 190 - (y * 10)

try:
    file = open("index.html", "a")
    # Indica donde empieza a escribir el script
    try:
        file.write("\n<!-- ESCRIBE EL SCRIPT -->\n")
        # escribe los divs al final del archivo
        for x in range(0, 20):
            for y in range(19, -1, -1):
                file.write('<div id="pixel-'+str(x)+'-'+str(y)+'" onclick="handlerClick(this)" style="position: absolute; display: block; left: '+ str(calculaPixelLeft(x)) +'px; top: '+ str(calculaPixelTop(y)) +'px; width: 10px; height: 10px; border: 1px solid rgb(204, 204, 204); background-color: yellow;"></div>\n')
    except Exception as e:
        print("Error al escribir\n"+str(e))
    finally:
        file.close()
except Exception as e:
    print("Error al abrir el archivo")
finally:
    file.close()