var canvas = document.getElementById("canvas");

// Algoritmo a ultilizar
var algoritmo = "scope"

// Datos sobre el click
var pixelsSeleccionados = 0;
var x1 = -1;
var y1 = -1;
var x2 = -1;
var y2 = -1;

/**
 * Devuelve las coordenadas de un pixel dado un <div> del canvas
 * @param {String} ID del div
 * @return coordenadas en array [x, y]
 */
function getCoordenadas(pixelId) {
    // Forma del ID: pixel-x-y
    var coordenadas = new Array(2);
    coordenadas[0] = parseInt( pixelId.slice(pixelId.indexOf("-") + 1, pixelId.lastIndexOf("-")) ) ;
    coordenadas[1] = parseInt( pixelId.slice(pixelId.lastIndexOf("-") + 1) );
    return coordenadas;
}

/**
 * Devuelve el id de un punto dadas sus coordenadas
 * @param coordenadas del punto en array [x, y]
 * @return string con el <div>
 */
function getId(coordenadas) {
    return "pixel-" + coordenadas[0] + "-" + coordenadas[1];
}

/**
 * Funcion que calcula el slope interception
 * sin modificaciones
 */
function algoritmoSlopeInterception() {
    var x = x1;
    var y = y1;
    var listaPixeles = [];
    var m = (y2 - y1) / (x2 - x1);
    var b = y1 - m*x1;

    while (x <= x2) {
        listaPixeles.push([x,y]);
        x += 1;
        y = Math.round(m*x + b);
    }

    return listaPixeles;
}

/**
 * Funcion que calcula el algoritmo slope interception 
 * en caso de que la y inicial e y final sean iguales
 * @return Lista de pixeles que conforman la recta
 */
function slopeMod1() {
    var x = x1;
    var lista = [];
    while (x <= x2) {
        lista.push([x, y1]);
        x = x + 1;
    }
    return lista;
}

/**
 * Funcion que calcula el algoritmo slope interception
 * en caso de que la x inicial y final sean iguales
 * @return Lista de pixeles que conforman la recta
 */
function slopeMod2() {
    var y = y1;
    var lista = [];
    while (y <= y2) {
        lista.push([x1, y]);
        y = y + 1;
    }
    return lista;
}

/**
 * Funcion que intercambia los puntos iniciales para 
 * hacer funcionar el algoritmo slope interception en el
 * segundo cuadrante
 */
function intercambiaPuntosIniciales() {
    var aux = x2;
    x2 = x1;
    x1 = aux;
    aux = y2;
    y2 = y1;
    y1 = aux;
}

/**
 * Funcion que intercambia en los puntos x1,y1 e x2,y2
 * las coordenadas x e y 
 */
function intercambiaXY() {
    var aux = x1;
    x1 = y1;
    y1 = aux;
    aux = x2;
    x2 = y2;
    y2 = aux;
}

function algoritmoSlopeInterceptionMod() {
    if(x2 < x1) {
        intercambiaPuntosIniciales();
    }
    if (y1 == y2) {
        return slopeMod1();
    }
    if (x1 == x2) {
        return slopeMod2();
    }
    var x = x1;
    var y = y1;
    var listaPixeles = [];
    var dx = x2 - x1;
    var dy = y2 - y1;
    var m = dy / dx;
    var b = y1 - m * x1;
    var variablesIntercambiadas = false;
    if (m > 1) {    // Mod 3 -> Invierte la pendiente, recalcula la B y recorre las Ys calculando las Xs
        m = 1/m;
        b = x1 - m * y1
        while (y <= y2) {
            listaPixeles.push([x,y]);
            y = y + 1;
            x = Math.round(m * y + b);
        }
        return listaPixeles;
    }
    if (m < -1) {
        m = Math.abs(1/m);
        b = x1 - m * y1;
        var recorre = y;
        while(y >= y2) {
            listaPixeles.push([x,y]);
            y = y - 1;
            recorre = recorre + 1;
            x = Math.round(m * recorre + b);
        }
        return listaPixeles;
    }
    while (x <= x2) {
        listaPixeles.push([x, y]);
        x = x + 1;
        y = Math.round(m * x + b);
    }
    return listaPixeles;
}

function algoritmoDDA() {
    var listaPixeles = [];
    var dx = x2 - x1;
    var dy = y2 - y1;
    var m = Math.max(Math.abs(dx), Math.abs(dy));
    var dxMod = dx / m;
    var dyMod = dy / m;
    var x = x1 + 0.5;
    var y = y1 + 0.5;
    for (i = 0; i <= m; i++) {
        listaPixeles.push([Math.floor(x), Math.floor(y)]);
        x = x + dxMod;
        y = y + dyMod;
    }
    return listaPixeles;
}

function signoParaBresenham(numero) {
    if (numero > 0) {
        return 1;
    }else if(numero < 0) {
        return -1;
    }else {
        return 0;
    }
}

function algoritmoBresenhamGeneral() {
    var listaPixeles = [];
    var x = x1;
    var y = y1;
    var dx = Math.abs(x2 - x1);
    var dy = Math.abs(y2 - y1);
    // Se calcula el signo de dx y dy
    var s1 = signoParaBresenham(x2 - x1);
    var s2 = signoParaBresenham(y2 - y1);
    var intercambio;    // Booleano que dice si se han intercambiado las variables
    if (dy > dx) {
        var aux = dx;
        dx = dy;
        dy = aux;
        console.log(dx, dy);
        intercambio = true;
    } else {
        intercambio = false;
    }    
    var m = 2*dy - dx;
    for(i = 1; i <= dx; i++){
        listaPixeles.push([x,y]);
        // Comprueba la pendiente
        if (m >= 0) {
            if(intercambio) {
                x += s1;
            }else {
                y += s2;
            }
            m -= 2*dx;
        }
        // Si la pendiente es negativa
        if (intercambio) {
            y += s2;
        }else {
            x += s1;
        }
        m += 2*dy;
    }

    return listaPixeles;
    
}

/**
 * Funcion que elige el algoritmo a utilizar
 * @return array de pixeles que pintar
 */
function eligeAlgoritmo() {
    //alert("Aplicar algoritmo " + algoritmo + " desde " + x1 + ", " + y1 + " hasta " + x2 + ", " + y2);
    console.log(algoritmo);
    switch (algoritmo) {
        case "slope":
            return algoritmoSlopeInterception();
            break;
        case "slope-mod":
            return algoritmoSlopeInterceptionMod();
            break;
        case "dda":
            return algoritmoDDA();
            break;
        case "bresenham":
            return algoritmoBresenhamGeneral();
            break;
        default:
            console.log("Fallo en la seleccion del algoritmo");
            break;
    }
}

/**
 * Funcion que pinta los pixeles en el canvas
 * @param {Array} pixeles 
 */
function pinta(pixeles) {
    var div;
    for (i = 0; i < pixeles.length; i++) {
        div = getId(pixeles[i]);
        document.getElementById(div).style.backgroundColor = "black";
    }
    return;
}

/**
 * Funcion que lleva a cabo la impresion de lineas 
 */
function gestionaPinta() {
    var listaPixeles = eligeAlgoritmo();    // Obtiene la lista de pixeles a dibujar

    if (listaPixeles.length <= 0) {  // Si la lista esta vacia
        alert("El array con la lista de pixeles está vacio");
        return -1;
    } else {
        console.log(listaPixeles);
        // Si tenemos lista de pixeles los pintamos
        pinta(listaPixeles);
    }
    return;
}

// Coge el evento del click
function handlerClick(pixel) {
    // Comprueba que pixel se ha pulsado
    var pixelPulsado = pixel.id
    // Tratamiento del id para obtener coordenadas
    var coordenadas = getCoordenadas(pixelPulsado)
    /* 3 posibilidades: No se ha pulsado aun ningun pixel,
     Se ha pulsado 1 pixel ó se han pulsado los dos pixeles */
    if (pixelsSeleccionados == 0) {
        x1 = coordenadas[0];
        y1 = coordenadas[1];
        pixelsSeleccionados++;
    } else if (pixelsSeleccionados == 1) {
        x2 = coordenadas[0];
        y2 = coordenadas[1];
        pixelsSeleccionados++;
        gestionaPinta();
    } else {
        return;
    }
}
/**
 * Funcion que determina que algoritmo se ha seleccionado
 * @param nombre Nombre de los botones radio
 * @return El algoritmo seleccionado
 */
function getAlgoritmoSeleccionado(nombre) {
    // Obtiene el formulario
    var form = document.getElementById("formAlg");
    var botones = form.elements[nombre];    // Obtiene los botones radio
    // Recorre la lista y devuelve el marcado
    for (let i = 0; i < botones.length; i++) {
        if (botones[i].checked) {
            return botones[i].value;
        }
    }
    return -1;
}

/**
 * Funcion handler para la seleccion de algoritmo
 */
function handlerAlgoritmos() {
    algoritmo = getAlgoritmoSeleccionado("alg");
}

/**
 * Funcion que limpia el canvas
 */
function clear() {
    console.log("Limpiando...");
    x1 = -1;
    x2 = -1;
    y1 = -1;
    y2 = -1;
    pixelsSeleccionados = 0;
    var div;
    for(let i = 0; i < 20; i++) {
        for(let j = 0; j < 20; j++) {
            div = document.getElementById(getId([i,j]));
            div.style.backgroundColor = "yellow";
        }
    }
}

document.getElementById("clear").addEventListener("click", function(){
    clear();
});